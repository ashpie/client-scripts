#!/bin/sh

file_name=$(echo ~/Pictures/Screenshots/)
base_name=$(date +%F_%H-%M-%S)
base_name+=".png"

file_name+=$base_name

flameshot gui -r > $file_name

sum1=$(cat $file_name | md5sum)
sum2=$(echo -e "screenshot aborted" | md5sum)

if test "$sum1" = "$sum2"; then
	notify-send "Screenshot aborted" "$url" --icon=flameshot.svg
	exit
fi

url=$(sh $(dirname $0)/upload_file.sh $file_name)

echo $url | xclip -selection clipboard

notify-send "File upload complete" "$url" --icon=flameshot.svg
