#!/bin/sh

latest_screenshot=$(ls -t ~/Pictures/Screenshots/ | head -1)

file_name=$(echo ~/Pictures/Screenshots/)
base_name=$latest_screenshot

file_name+=$base_name


sum1=$(cat $file_name | md5sum)
sum2=$(echo -e "screenshot aborted" | md5sum)

if test "$sum1" = "$sum2"; then
	notify-send "Last screenshot was aborted" "$url" --icon=flameshot.svg
	exit
fi

url=$(sh $(dirname $0)/upload_file.sh $file_name)

echo $url | xclip -selection clipboard

notify-send "File upload complete" "$url" --icon=flameshot.svg
