#!/bin/sh

UPDATES=$(arch-audit --upgradable --show-testing | sed "{s/\n/<br>/g}")

if test "$1" != "true"
then
	UPDATES=$(echo -e "$UPDATES" | grep -v "testing repos\!")
fi

echo -e "$UPDATES"

if [ ! -z "$UPDATES" ]; then
	sudo -u arisu DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus notify-send -u critical -t 86400000 -i update-high "Security updates are available" "$UPDATES" --app-name="Security Updates Check"
fi
