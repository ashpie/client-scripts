#!/bin/sh

local_file=$1
slug=$2
token=$(cat "$HOME/.ily_token")

if test -z "$slug"; then
  method=POST
else
  method=PUT
fi

curl -X $method -H 'Accept: text/plain' \
  -H "Authorization: $token" \
  -F 'type=file' \
  -F "upload=@$local_file" \
  -F 'ttl=2592000' \
  "https://ily.li/$slug"
